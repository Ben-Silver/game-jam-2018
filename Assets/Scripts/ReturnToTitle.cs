﻿/*-----------------------------------------------------*/
/*                   ReturnToTitle.cs                  */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReturnToTitle : MonoBehaviour
{
    /* Public Variables */
    public Text scoreText;                      // The current score on the screen
    public int score;                           // Holds the current score to be represented in scoreText

    // Use this for initialization
    void Start()
    {
        // Check to see if "HighScore" currently exists in PlayerPrefs
        if (PlayerPrefs.HasKey("Score"))
        {
            // Get the high score from PlayerPrefs
            score = PlayerPrefs.GetInt("Score");

            // Set the scoreText according to the player's score
            scoreText.text = "You were fired!\nScore: " + score;
        }
    }

    /// <summary>
    /// Function to return to the title screen
    /// </summary>
    public void ToTitle()
    {
        // Load the title scene
        SceneManager.LoadScene("Title");
    }
}
