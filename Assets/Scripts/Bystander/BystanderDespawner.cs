﻿/*-----------------------------------------------------*/
/*                BystanderDespawner.cs                */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BystanderDespawner : MonoBehaviour
{
    /// <summary>
    /// Function to despawn bystanders
    /// </summary>
    /// <param name="other">The collider of the gameObject that entered the proximity</param>
    private void OnTriggerEnter(Collider other)
    {
        // Check to make sure the object being despawned is a bystander
        if (other.gameObject.tag == "Bystander" && other.GetComponent<BystanderStateMachine>().bystanderStates == BystanderStateMachine.BystanderStates.LEAVING)
        {
            // Destroy/despawn the bystander
            Destroy(other.gameObject);
        }
    }
}
