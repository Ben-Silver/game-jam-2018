﻿/*-----------------------------------------------------*/
/*                 BystanderSpawner.cs                 */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BystanderSpawner : MonoBehaviour
{
    /* Public Variables */
    public GameObject bystander;                // The book prefab
    public GameObject newBystander;             // Used to create instances of books
    public float timer;                         // The spawn timer
    public float maxTimer;                      // The maximum value of the spawn timer
    public float cooldown;                      // The cooldown time after spawning a bystander
    public bool spawnedBystander;               // Controls when a new bystander can be spawned

    // Use this for initialization
    void Start()
    {
        // Reset the timer
        timer = 0.0f;

        // Randomise the maximum timer
        maxTimer = Random.Range(0, 32);

        // Reset the cooldown
        cooldown = 0.0f;

        // Reset spawnedBystander
        spawnedBystander = true;
    }

    // Update is called once per frame
    void Update()
    {
        // Check to see if a bystander has been spawned
        if (spawnedBystander)
        {
            // Check the current cooldown
            if (cooldown >= 4)
            {
                // Reset the cooldown time
                cooldown = 0.0f;

                // A new bystander can be spawned again
                spawnedBystander = false;
            }
            else
            {
                // Increase the cooldown by a little bit
                cooldown += Time.deltaTime;
            }
        }
        else
        {
            // Spawn a new bystander
            SpawnBystander();
        }
    }

    /// <summary>
    /// Function to spawn a new instance of a bystander
    /// </summary>
    public void SpawnBystander()
    {
        // Check the current value of the timer
        if (timer <= maxTimer)
        {
            // Increase the timer by a little bit
            timer += Time.deltaTime;
        }
        else
        {
            // Create a new instance of the bystander (spawn a new bystander)
            newBystander = Instantiate(bystander, transform.position, transform.rotation);

            // A bystander has just spawned
            spawnedBystander = true;

            // Reset the timer
            timer = 0.0f;

            // Rerandomise the maximum timer
            maxTimer = Random.Range(0, 32);
        }
    }
}
