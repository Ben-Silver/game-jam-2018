﻿/*-----------------------------------------------------*/
/*                 AnimateBystander.cs                 */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateBystander : MonoBehaviour
{
    /* Public Variables */
    public BystanderStateMachine bsm;
    public Animator anim;

    // Use this for initialization
    void Start()
    {
        // Get the bystander state machine on the gameObject
        bsm = GetComponent<BystanderStateMachine>();

        // Get the animator component on the gameObject
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("walking", true);
    }
}
