﻿/*-----------------------------------------------------*/
/*            BystanderTargetDestination.cs            */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BystanderTargetDestination : MonoBehaviour
{
    /* Serves no function other than as a component to be fetched */
}
