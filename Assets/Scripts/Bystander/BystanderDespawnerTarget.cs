﻿/*-----------------------------------------------------*/
/*             BystanderDespawnerTarget.cs             */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BystanderDespawnerTarget : MonoBehaviour
{
    /* Serves no function other than as a component to be fetched */
}
