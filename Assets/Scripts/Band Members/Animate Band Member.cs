﻿/*-----------------------------------------------------*/
/*                 AnimateBandMember.cs                */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateBandMember : MonoBehaviour
{
    /* Public Variables */
    public Animator anim;
    
	// Use this for initialization
	void Start()
    {
        // Get the animator component on the gameObject
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}
}
