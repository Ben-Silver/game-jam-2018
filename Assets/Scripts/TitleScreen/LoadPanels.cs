﻿/*-----------------------------------------------------*/
/*                     LoadPanels.cs                   */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPanels : MonoBehaviour
{
    /* Public Variables */
    public GameObject previousPanel;            // The previous panel that will be muted on load
    public GameObject nextPanel;                // The next panel to be loaded

    /// <summary>
    /// Function to load the high score
    /// </summary>
    public void LoadNewPanel()
    {
        // Make the main menu panel invisible
        previousPanel.SetActive(false);

        // Make the high score panel visible
        nextPanel.SetActive(true);
    }

    /// <summary>
    /// Function to go back to the title screen
    /// </summary>
    public void BackToTitle()
    {
        // Make the main menu panel invisible
        previousPanel.SetActive(true);

        // Make the high score panel visible
        nextPanel.SetActive(false);
    }
}
