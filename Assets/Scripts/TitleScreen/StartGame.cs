﻿/*-----------------------------------------------------*/
/*                     StartGame.cs                    */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    /// <summary>
    /// Function to load a new game
    /// </summary>
    public void StartNewGame()
    {
        // Load the library scene
        SceneManager.LoadScene("Library");
    }
}
