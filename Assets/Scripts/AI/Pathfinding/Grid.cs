﻿/*-----------------------------------------------------*/
/*                       Grid.cs                       */
/*-----------------------------------------------------*/
/*      Retrieved by following a YouTube tutorial      */
/*                 by Sebastian Lague                  */
/* https://www.youtube.com/watch?v=-L-WgKMFuhE&index=1&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW&t=445s */
/*-----------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{
    /* Public Variables */
    public bool displayGridGizmos;                          // True if we want to visualise the grid on-screen
	public LayerMask unwalkableMask;                        // Determines which GameObjects in the scene cannot be passed through
	public Vector2 gridWorldSize;                           // The size of the grid
	public float nodeRadius;                                // the radius size of each node
    public TerrainType[] walkableRegions;                   // An array of different passable regions
	public int obstacleProximityPenalty = 10;               // The terrain penalty of an obstacle

    /* Private Variables */
    private Dictionary<int, int> walkableRegionsDict =      // A dictionary of walkable regions
        new Dictionary<int, int>();
    private LayerMask walkableMask;                         // Determines which GameObjects in the scene can be passed through
	private Node[, ] grid;                                  // An array of Nodes
	private float nodeDiameter;                             // The diameter of each node
	private int gridSizeX, gridSizeY;                       // The X and Y size of the grid
    private int penaltyMin = int.MaxValue;                  // The minimum terrain penalty
    private int penaltyMax = int.MinValue;                  // The maximum terrain penalty

    /* Getter for MaxSize */
	public int MaxSize
	{
		get { return gridSizeX * gridSizeY; }
	}

    // Called when the script instance is being loaded
    void Awake()
	{
        // Set nodeDiameter to nodeRadius x 2
		nodeDiameter = nodeRadius * 2;

        // Set gridSizeX to the width we set / nodeDiameter, all rounded to an int
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);

        // Set gridSizeY to the length we set / nodeDiameter, all rounded to an int
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        // Loop through for every terrain type in the walkableRegions array
        foreach (TerrainType region in walkableRegions)
        {
            // Set the value of walkableMask to equal the current value OR the region value
            walkableMask.value |= region.terrainMask.value;

            // Add the base of region terrain mask value & power 2 and region terrain penalty to the walkableRegionsDict list
            walkableRegionsDict.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainPenalty);
        }

        // Create the grid
		CreateGrid();
	}
	
	/* Function to create the grid */
	void CreateGrid()
	{
        // Set the grid size
		grid = new Node[gridSizeX, gridSizeY];

        // Keeping the grid aligned to the center
		Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        // Loop through from 0 to the length of the grid
		for (int x = 0; x < gridSizeX; x++)
		{
            // Loop through from 0 to the height of the grid
			for (int y = 0; y < gridSizeY; y++)
			{
                // Create the world point at the centre of the grid
				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
				bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));

                // Set the movement penalty to 0
                int movementPenalty = 0;

                // Create a new Ray taking in worldPoint + Vector3.up * 50 and Vector3.down
	            Ray ray = new Ray(worldPoint + Vector3.up * 50, Vector3.down);

                // Create a new RaycastHit
	            RaycastHit hit;

                // Check to see if the ray collides with anything of the walkableMask LayerMask
	            if (Physics.Raycast(ray, out hit, 100, walkableMask))
	            {
                    // Get the value from the walkableRegions dictionary
                    walkableRegionsDict.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);
	            }

                // Check to see if the seeker can walk on the surface
				if (!walkable)
				{
                    // Increase the movement penalty by the value of obstacleProximityPenalty
					movementPenalty += obstacleProximityPenalty;
				}

                // Create a new grid of nodes
				grid[x, y] = new Node(walkable, worldPoint, x, y, movementPenalty);
			}
		}

        // Blur the penalty map by 3
        BlurPenaltyMap(3);

    }

    /* Function to blur the penalty map */
    void BlurPenaltyMap(int blurSize)
    {
        int kernelSize = blurSize * 2 + 1;          // (blurSize * 2) + 1
        int kernelExtents = (kernelSize - 1) / 2;   // (kernalSize - 1) / 2

        int[,] penaltiesHorizontalPass = new int[gridSizeX, gridSizeY];
        int[,] penaltiesVerticalPass = new int[gridSizeX, gridSizeY];

        for (int y = 0; y < gridSizeY; y++)
        {
            for (int x = -kernelExtents; x < kernelExtents; x++)
            {
                int sampleX = Mathf.Clamp(x, 0, kernelExtents);
                penaltiesHorizontalPass[0, y] += grid[sampleX, y].movementPenalty;
            }

            for (int x = 1; x < gridSizeX; x++)
            {
                int removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, gridSizeX);
                int addIndex = Mathf.Clamp(x + kernelExtents, 0, gridSizeX - 1);

                penaltiesHorizontalPass[x, y] = penaltiesHorizontalPass[x - 1, y] - grid[removeIndex, y].movementPenalty
                                                                                  + grid[addIndex, y].movementPenalty;
            }
        }

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = -kernelExtents; y < kernelExtents; y++)
            {
                int sampleY = Mathf.Clamp(y, 0, kernelExtents);
                penaltiesVerticalPass[x, 0] += penaltiesHorizontalPass[x, sampleY];
            }

			int blurredPenalty = Mathf.RoundToInt((float)penaltiesVerticalPass[x, 0] / (kernelSize * kernelSize));
			grid[x, 0].movementPenalty = blurredPenalty;

            for (int y = 1; y < gridSizeY; y++)
            {
                int removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, gridSizeY);
                int addIndex = Mathf.Clamp(y + kernelExtents, 0, gridSizeY - 1);

                penaltiesVerticalPass[x, y] = penaltiesVerticalPass[x, y - 1] - penaltiesHorizontalPass[x, removeIndex]
                                                                                  + penaltiesHorizontalPass[x, addIndex];
                blurredPenalty = Mathf.RoundToInt((float)penaltiesVerticalPass[x, y] / (kernelSize * kernelSize));
                grid[x, y].movementPenalty = blurredPenalty;

                if (blurredPenalty > penaltyMax)
                {
                    penaltyMax = blurredPenalty;
                }

                if (blurredPenalty < penaltyMin)
                {
                    penaltyMin = blurredPenalty;
                }
            }
        }

    }

	public List<Node> GetNeighbours(Node node)
	{
		List<Node> neighbours = new List<Node>();
		
		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				if (x == 0 && y == 0)
				{
					continue;
				}
				
				int checkX = node.gridX + x;
				int checkY = node.gridY + y;
				
				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
				{
					neighbours.Add(grid[checkX, checkY]);
				}
			}
		}
		
		return neighbours;
	}

	public Node NodeFromWorldPoint(Vector3 worldPosition)
	{
		float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
		float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);
		
		int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
		int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
		return grid[x, y];
	}
	
	void OnDrawGizmos()
	{
		Gizmos.DrawWireCube (transform.position, new Vector3 (gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null && displayGridGizmos)
        {
            foreach (Node n in grid)
            {
                Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(penaltyMin, penaltyMax, n.movementPenalty));

                Gizmos.color = (n.walkable) ? Gizmos.color : Color.red;
                Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter));
            }
        }
	}

    [System.Serializable]
    public class TerrainType
    {
        public LayerMask terrainMask;
        public int terrainPenalty;
    }
}
