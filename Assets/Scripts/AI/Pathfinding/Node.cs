﻿/*-----------------------------------------------------*/
/*                       Node.cs                       */
/*-----------------------------------------------------*/
/*      Retrieved by following a YouTube tutorial      */
/*                 by Sebastian Lague                  */
/* https://www.youtube.com/watch?v=-L-WgKMFuhE&index=1&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW&t=445s */
/*-----------------------------------------------------*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Node : IHeapItem<Node>
{
    public bool walkable;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;
    public int movementPenalty;
    
    public int gCost;
    public int hCost;
    public Node parent;
    int heapIndex;

	public Node(bool _walkable, Vector3 _worldPosition, int _gridX, int _gridY, int _penalty)
    {
        walkable 		= _walkable;
        worldPosition 	= _worldPosition;
		gridX 			= _gridX;
		gridY 			= _gridY;
        movementPenalty = _penalty;
	}
	
    public int fCost
    {
    	get
    	{
    		return gCost + hCost;
    	}
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }

        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = fCost.CompareTo(nodeToCompare.fCost);

        if (compare == 0)
        {
            compare = hCost.CompareTo(nodeToCompare.hCost);
        }

        return -compare;
    }
}
