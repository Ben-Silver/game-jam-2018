﻿/*-----------------------------------------------------*/
/*                       Unit.cs                       */
/*-----------------------------------------------------*/
/*      Retrieved by following a YouTube tutorial      */
/*                 by Sebastian Lague                  */
/* https://www.youtube.com/watch?v=-L-WgKMFuhE&index=1&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW&t=445s */
/*-----------------------------------------------------*/

using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
    /* Constant Variables */
	const float minPathUpdateTime = 0.2f;           // The minimum time for the path to update
	const float pathUpdateMoveThreshhold = 0.5f;    // The movement threshhold for the path to update

    /* Public Variables */
    public Transform target;                        // The goal this GameObject is moving towards
	public float speed = 5.0f;                      // The speed at which the GameObject will move
	public float turnSpeed = 3.0f;                  // The turning speed of the GameObject
	public float turnDst = 2.5f;                    // The turning distance of the GameObject
    public float stoppingDst = 10.0f;               // The distance from the target that the GameObject will decelerate
    public Path path;                               // Reference to Path.cs
    public bool pathReached;                        // True when a path has successfully reached

    // Use this for initialization
    void Start()
	{
        // Update the path on initialization
		StartCoroutine(UpdatePath());
	}
	
    /* Function to be called when a path has been found */
	public void OnPathFound(Vector3[] waypoints, bool pathSuccessful)
	{
        // If the path was successful
		if (pathSuccessful)
		{
            // Set pathReached to true
            pathReached = pathSuccessful;

            // Set path to a new instance with updated values
            path = new Path(waypoints, transform.position, turnDst, stoppingDst);

            // Restart the FollowPath coroutine
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

    /* Function to Update the current path */
	IEnumerator UpdatePath()
	{
        // Check to see if the time since the level was loaded is less than 0.3
		if (Time.timeSinceLevelLoad < 0.3f)
		{
            // Wait for 0.3 seconds
			yield return new WaitForSeconds(0.3f);
		}

        // Access PathRequestManager to request a new path
        PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));

        // set squareMoveThreshhold to the value of pathUpdateMoveThreshhold squared
        float squareMoveThreshhold = pathUpdateMoveThreshhold * pathUpdateMoveThreshhold;

        // Set the old target position to the current target position
		Vector3 targetPosOld = target.position;

		while (true)
		{
            // Return the minimum path update time
			yield return new WaitForSeconds(minPathUpdateTime);

            // Check to see if the position of the squared length of (target - the target's old position)
			if ((target.position - targetPosOld).sqrMagnitude > squareMoveThreshhold)
			{
                // Fetch a new path from the PathRequestManager, passing in the current position and the current target position
				PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));

                // Set the old target position to the current target position
                targetPosOld = target.position;
			}
		}
	}

    /* Function to follow the path */ 
	IEnumerator FollowPath()
	{
        // A path is currently being followed
		bool followingPath = true;

        // Create a pathIndex
		int pathIndex = 0;

        // Move the GameObject through the first lookpoint
		transform.LookAt(path.lookPoints[0]);

        // Holds a percentage
		float speedPercent = 1.0f;

        // While the seeker is following its path
		while (followingPath)
		{
            // Temporary Vector2 to hold the X and Z positions of the seeker
			Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);

            // While the seeker is passing through the turn boundaries
			while (path.turnBoundaries[pathIndex].HasCrossedLine(pos2D))
			{
                // Check to see if pathIndex is equal to the finishLineIndex
				if (pathIndex == path.finishLineIndex)
				{
                    // Set followingPath to false
					followingPath = false;
					break;
				}
				else
				{
                    // Increment pathIndex
					pathIndex++;
				}
			}

            // Check to see if the path is being followed
			if (followingPath)
			{
                // Check to see if pathIndex is more than or equal to the slowDownIndex and check to see if stoppingDst is more than 0
                if (pathIndex >= path.slowDownIndex && stoppingDst > 0)
				{
                    // Clamp the speed percentage between 0 and 1
					speedPercent = Mathf.Clamp01(path.turnBoundaries[path.finishLineIndex].DistanceFromPoint(pos2D) / stoppingDst);

                    // Check to see if speedPercent is less than 0.01
					if (speedPercent < 0.01f)
					{
                        // Set followingPath to false
						followingPath = false;
					}
				}

                // Set the target rotation of the seeker so it is facing forward down the travelled path
				Quaternion targetRotation = Quaternion.LookRotation(path.lookPoints[pathIndex] - transform.position);

                // Set the normalised value from the current rotation to the targetRotation as the current rotation
				transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * turnSpeed);

                // Move the seeker along the path
				transform.Translate(Vector3.forward * Time.deltaTime * speed * speedPercent, Space.Self);
			}

            // Return nothing
            yield return null;
		}
	}
	
    /* Function to draw the path with gizmos for debugging purposes */
	public void OnDrawGizmos()
	{
        // Check that a path exists
		if (path != null)
		{
            // Draw gizmos along the current path
			path.DrawWithGizmos();
		}
	}
}
