﻿/*-----------------------------------------------------*/
/*                BoyBandStateMachine.cs               */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoyBandStateMachine : MonoBehaviour
{
    /* Public Variables */
    public enum BoyBandStates
    {
        PLAYING,
        NOTPLAYING,
        SCAREDOFF
    }                // All possible states for the boy band
    public BoyBandStates boyBandStates;         // The current state the boy band is in
    public Image quietPleaseImg;                // QuietPlease image
    public GameObject noiseLevelPanel;          // Panel containing the noise level progress bar
    public Image noiseLevelBar;                 // A progress bar depicting noise level
    public Vector3[] positions;                 // Array of possible spawn positions
    public float timer;                         // The timer that controls the spawn
    public static float singTimer;              // Increases the longer the boy band is allowed to cause a racket
    public float quietTimer;                    // How long the QuietPlease image will show for
    public float noiseLevel;                    // The current noise level
    public float noiseLevelMax = 12;            // The maximum noise level

	// Use this for initialization
	void Start()
    {
        // Set the current state to NOTPLAYING
        boyBandStates = BoyBandStates.NOTPLAYING;

        // Reset the timer
        timer = 0.0f;

        // Reset the singTimer
        singTimer = 0.0f;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Set the noise level progress bar according to the sing timer
        noiseLevelBar.fillAmount = (singTimer + 0.01f) / (noiseLevelMax - 2);

        // Determine what happens depending on the current state
        switch (boyBandStates)
        {
            // When the current state is NOTPLAYING
            case (BoyBandStates.NOTPLAYING):

                // Check the value of the timer
                if (timer <= noiseLevelMax)
                {
                    // Increase the timer by a little bit
                    timer += Time.deltaTime;
                }
                else
                {
                    // Set the random position
                    transform.position = positions[Random.Range(0, positions.Length)];

                    // Reset the timer
                    timer = 0.0f;

                    // Show the noise level panel
                    noiseLevelPanel.gameObject.SetActive(true);

                    // Change the rotations
                    ChangeRotations();

                    // Move to the PLAYING state
                    boyBandStates = BoyBandStates.PLAYING;
                }

            break;

            // When the current state is PLAYING
            case (BoyBandStates.PLAYING):

                // Increase the singTimer by a little bit
                singTimer += Time.deltaTime;// * ((ScoreManager.score + 1) / 8);

            break;

            // When the current state is PLAYING
            case (BoyBandStates.SCAREDOFF):

                // Hide the noise level panel
                noiseLevelPanel.gameObject.SetActive(false);

                // Show the QuietPlease logo for 4 seconds
                QuietPlease(4.0f);

                // Increase timer whenever a book is placed

            break;
        }
	}

    /// <summary>
    /// Function to reset the state machine when the player enter's their vicinity
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // If it's the player that collided with them
        if (other.gameObject.tag == "Player")
        {
            // Reset the positon
            transform.position = new Vector3(10, -10, 10);

            // Reset the quietTimer
            quietTimer = 0.0f;
            
            // Reset the singTimer
            singTimer = 0.0f;
            
            // Reset the state
            boyBandStates = BoyBandStates.SCAREDOFF;
        }
    }

    /// <summary>
    /// Function to show the QuietPlease logo for a number of seconds
    /// </summary>
    /// <param name="maxTime">How long the logo will show for</param>
    public void QuietPlease(float maxTime)
    {
        // Check the value of quietTimer
        if (quietTimer < maxTime)
        {
            // Make the QuietPlease logo visible
            quietPleaseImg.gameObject.SetActive(true);

            // Increase quietTimer by a little bit
            quietTimer += Time.deltaTime;
        }
        else
        {
            // Make the QuietPlease logo invisible
            quietPleaseImg.gameObject.SetActive(false);

            // Reset the state
            boyBandStates = BoyBandStates.NOTPLAYING;
        }
    }

    /// <summary>
    /// Function to change rotations depending on position
    /// </summary>
    public void ChangeRotations()
    {
        if (transform.position == positions[0])
        {
            transform.Rotate(0, 90, 0);
        }
        else if (transform.position == positions[1])
        {
            transform.Rotate(0, -90, 0);
        }
        else if (transform.position == positions[2])
        {
            transform.Rotate(0, 90, 0);
        }
        else if (transform.position == positions[3])
        {
            transform.Rotate(0, -90, 0);
        }
        else if (transform.position == positions[4])
        {
            transform.Rotate(0, 0, 0);
        }
        else if (transform.position == positions[5])
        {
            transform.Rotate(0, 0, 0);
        }
    }
}
