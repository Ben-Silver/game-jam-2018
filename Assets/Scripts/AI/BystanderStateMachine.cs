﻿/*-----------------------------------------------------*/
/*               BystanderStateMachine.cs              */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BystanderStateMachine : MonoBehaviour
{
    /* Public Variables */
    public enum BystanderStates
    {
        WALKING,
        IDLE,
        LEAVING
    }              // All possible states for the boy band
    public BystanderStates bystanderStates;     // The current state the bystander is in
    public Unit pathfinder;                     // Reference to Unit class
    public float waitingTime;                   // How long the bystander has been idle for
    public float maxWaitingTime;                // How long the bystander will remain idle
    public BystanderTargetDestination[] dest;   // An array of all the destinations in the scene
    public BystanderDespawnerTarget[] despawner;// An array of all the despawners in the scene
    public int destinationIndex;                // The index of the destination the bystander will head towards
    public int despawnerIndex;                  // The index of the despawner the bystander will head towards
    public bool walking = false;                // True when the bystander is walking

    // Before initialization
    void Awake()
    {
        // Get all the despawners in the scene and load them into the array
        dest = FindObjectsOfType<BystanderTargetDestination>();

        // Choose a random index to be a reference to the destination the bystander will move towards
        destinationIndex = Random.Range(0, dest.Length);
        
        // Get the Unit class on the bystander
        pathfinder = GetComponent<Unit>();

        // Preset the pathfinder's target
        pathfinder.target = dest[destinationIndex].transform;
    }

    // Use this for initialization
    void Start()
    {
        // Reset waitingTime
        waitingTime = 0.0f;

        // Set the max waiting time to a random number
        maxWaitingTime = Random.Range(4, 32);

        // Get all the despawners in the scene and load them into the array
        despawner = FindObjectsOfType<BystanderDespawnerTarget>();
    }

    // Update is called once per frame
    void Update()
    {
        // Determine what happens depending on the current state
        switch (bystanderStates)
        {
            // When the current state is WALKING
            case (BystanderStates.WALKING):

                // If the bystander has reached its destination
                if (pathfinder.pathReached)
                {
                    // Move to the IDLE state
                    bystanderStates = BystanderStates.IDLE;
                }

            break;

            // When the current state is IDLE
            case (BystanderStates.IDLE):

                // Check how long the bystander has been idle for
                if (waitingTime < maxWaitingTime)
                {
                    // Increase waitingTime by a little bit
                    waitingTime += Time.deltaTime;
                }
                else
                {
                    // Reset waitingTime
                    waitingTime = -0.01f;

                    // Choose a random index to be a reference to the despawner the bystander will move towards
                    despawnerIndex = Random.Range(0, despawner.Length);

                    // Move to the LEAVING state
                    bystanderStates = BystanderStates.LEAVING;
                }

            break;

            // When the current state is LEAVING
            case (BystanderStates.LEAVING):

                // Move the bystander towards one of the exits
                pathfinder.target = despawner[despawnerIndex].transform;

            break;
        }
    }
}
