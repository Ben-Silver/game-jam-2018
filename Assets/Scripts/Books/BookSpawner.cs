﻿/*-----------------------------------------------------*/
/*                    BookSpawner.cs                   */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookSpawner : MonoBehaviour
{
    /* Public Variables */
    public GameObject book;                     // The book prefab
    public GameObject newBook;                  // Used to create instances of books
    public float timer;                         // The spawn timer
    public float cooldown;                      // The cooldown time between generated books
    public static bool spawnedBook;             // Whether a book has been spawned or not
    public Material[] material;                 // Array of material prefabs to assign to each book instance
    
    // Use this for initialization
    void Start()
    {
        // Reset spawnedBook
        spawnedBook = false;

        // Reset the timer
        timer = 0.0f;

        // Spawn books
        SpawnBookOnStart();
    }
	
	// Update is called once per frame
	void Update()
    {
        // Check to see if a book has already spawned
		if (spawnedBook)
        {
            // Check the current cooldown
            if (cooldown >= 4)
            {
                // Reset the cooldown time
                cooldown = 0.0f;

                // A new book can be spawned again
                spawnedBook = false;
            }
        }
        else
        {
            // Function call to spawn a new book
            SpawnBook();
        }
	}

    /// <summary>
    /// Function to spawn a new instance of a book
    /// </summary>
    public void SpawnBook()
    {
        // Check the current value of the timer
        if (timer <= 10)
        {
            // Increase the timer by a little bit
            timer += Time.deltaTime;
        }
        else
        {
            // Create a new instance of the book (spawn a new book)
            newBook = Instantiate(book, transform.position, transform.rotation);

            // Generate a random material and assign it to the material variable in HoldObject
            newBook.GetComponent<HoldObject>().material = material[Random.Range(0, material.Length)];

            //  A new book has been spawned
            spawnedBook = true;

            // Reset the timer
            timer = 0.0f;
        }
    }

    /// <summary>
    /// Function to spawn a new instance of a book in void Start()
    /// </summary>
    public void SpawnBookOnStart()
    {
        // Create a new instance of the book (spawn a new book)
        newBook = Instantiate(book, transform.position, transform.rotation);

        // Generate a random material and assign it to the material variable in HoldObject
        newBook.GetComponent<HoldObject>().material = material[Random.Range(0, material.Length)];
    }
}
