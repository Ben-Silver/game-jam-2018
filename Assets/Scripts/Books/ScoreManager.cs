﻿/*-----------------------------------------------------*/
/*                   ScoreManager.cs                   */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    /* Public Variables */
    public HighScoreManager scoreManager;       // Reference to HighSchoolManager class
    public Text scoreText;                      // The score represented in the UI
    public Text livesText;                      // The lives represented in the UI
    public static int score;                    // The current internal score
    public static int lives;                    // The current remaining lives
    
    // Use this for initialization
    void Start()
    {
        // Reset score
        score = 0;

        // Set the number of lives
        lives = 3;
	}
	
	// Update is called once per frame
	void Update()
    {
        // Constantly update the UI with the current score
        scoreText.text = "Score: " + score;

        // Constantly update the UI with the current lives
        livesText.text = "Warnings: " + lives;

        // Check the game state so far
        GameOver();

        // Check how long the boy band has been singing for
        if (BoyBandStateMachine.singTimer >= 8.0f)
        {
            // Decrease the player's lives by 1
            lives -= 1;

            // Reset the boy band singTimer
            BoyBandStateMachine.singTimer = 0.0f;
        }
    }

    /// <summary>
    /// Function to load the gameover screen
    /// </summary>
    public void GameOver()
    {
        // Check to see if the player has run out of lives
        if (lives == 0)
        {
            // Check the player's score against the high score
            SetHighScore();

            // Load the gameover screen
            SceneManager.LoadScene("GameOver");
        }
    }

    /// <summary>
    /// Function to set the high score
    /// </summary>
    public void SetHighScore()
    {
        // Set a new instance of HighSchoolManager
        scoreManager = new HighScoreManager();

        // Check to see if the current score is greater than the current high score
        if (score > scoreManager.highScore)
        {
            // Set the new high score
            PlayerPrefs.SetInt("HighScore", score);
        }

        // Set the player's current score in PlayerPrefs to be used in the GameOver screen
        PlayerPrefs.SetInt("Score", score);
    }
}
