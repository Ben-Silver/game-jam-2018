﻿/*-----------------------------------------------------*/
/*                    HoldObject.cs                    */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldObject : MonoBehaviour
{
    /* Public Variables */
    public Transform playerTransform;           // The transform of the player/hand
    public bool holdingObject = false;          // Whether the gameObject is being held by the player
    public bool alreadyHeld = false;            // Whether the gameObject has already been held and prevents other books from being moved all at once
    public Material material;                   // Variable to hold a material assigned from the book spawner
    
	// Use this for initialization
	void Start()
    {
        // Assign the material stored in matieral variable to the Renderer component on the book
        gameObject.GetComponent<Renderer>().material = material;
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Check to see if the player is holding the object
        if (PlayerController.holdingObject && !alreadyHeld)
        {
            // Set the parent of the gameObject to be the player
            gameObject.transform.parent = playerTransform;
        }
        else
        {
            // Set the gameObject to have no parent
            gameObject.transform.parent = null;
        }
	}

    /// <summary>
    /// Function for when an object collides with the book
    /// </summary>
    /// <param name="other">The object that collided with this gameObject</param>
    private void OnTriggerEnter(Collider other)
    {
        // If the collider is the player, isn't holding something and the gameObject hasn't already been picked up
        if (other.gameObject.tag == "PlayerHand" && !PlayerController.holdingObject && !alreadyHeld)
        {
            // Set the "player" transform to the collider transform
            playerTransform = other.transform;

            // Set the position of the gameObject to be in front of the player
            gameObject.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, playerTransform.position.z);

            // The player is now holding the object
            PlayerController.holdingObject = true;

            // The book spawner needs to spawn a new book
            BookSpawner.spawnedBook = false;
        }

        // If the collider is the bookshelf and the player is holding the gameObject
        if (other.gameObject.tag == "Bookshelf" && PlayerController.holdingObject)
        {
            // The player is no longer holding the gameObject
            PlayerController.holdingObject = false;

            // The player cannot pick up this object again
            alreadyHeld = true;

            // Destroy the book
            Destroy(gameObject);

            // Increase the score in the ScoreManager by 1
            ScoreManager.score += 1;
        }
    }
}
