﻿/*-----------------------------------------------------*/
/*              BookMaterialRandomiser.cs              */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookMaterialRandomiser : MonoBehaviour
{
    /* Public Variables */
    public Material[] material;
    
    // Use this for initialization
    void Start()
    {
        // Assign a random material to the Renderer component on the book
        gameObject.GetComponent<Renderer>().material = material[Random.Range(0, material.Length)];
    }
}
