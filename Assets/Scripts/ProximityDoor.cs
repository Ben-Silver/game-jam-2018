﻿/*-----------------------------------------------------*/
/*                   ProximityDoor.cs                  */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityDoor : MonoBehaviour
{
    /* Public Variables */
    public GameObject doorObject;
    public bool doorOpen = false;

    /// <summary>
    /// Function for when the player enters the door's proximity
    /// </summary>
    /// <param name="other">The gameObject that enters the proximity</param>
    private void OnTriggerEnter(Collider other)
    {
        // Check to see if it was the player that entered the door's proximity
        if ((other.gameObject.tag == "Player" || other.gameObject.tag == "PlayerHand") && !doorOpen)
        {
            // Set the door to open
            doorOpen = true;

            // Loop 90 times
            for (int i = 0; i < 90; i++)
            {
                // Rotate the door object by -1 degrees
                doorObject.transform.Rotate(0, 0, -1);
            }
        }
    }

    /// <summary>
    /// Function for when the player leaves the door's proximity
    /// </summary>
    /// <param name="other">The gameObject that leaves the proximity</param>
    private void OnTriggerExit(Collider other)
    {
        // Check to see if it was the player that exited the door's proximity
        if ((other.gameObject.tag == "Player" || other.gameObject.tag == "PlayerHand") && doorOpen)
        {
            // Set the door to open
            doorOpen = false;

            // Loop 90 times
            for (int i = 0; i < 90; i++)
            {
                // Rotate the door object by 1 degree
                doorObject.transform.Rotate(0, 0, 1);
            }
        }
    }
}
