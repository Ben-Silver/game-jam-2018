﻿/*-----------------------------------------------------*/
/*                   LoadHighScore.cs                  */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadHighScore : MonoBehaviour
{
    /* Public Variables */
    public HighScoreManager scoreManager;       // Reference to HighScoreManager class
    public Text highScoreText;                  // The high score on the screen
    public int highScore;                       // Holds the high score from PlayerPrefs
    
	// Use this for initialization
	void Start()
    {
        // Set a new instance of HighScoreManager
        scoreManager = new HighScoreManager();

        // Check to see if "HighScore" currently exists in PlayerPrefs
        if (PlayerPrefs.HasKey("HighScore"))
        {
            // Get the high score from PlayerPrefs
            highScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            // Default to 0
            highScore = 0;
        }

        // Show the high score on-screen
        highScoreText.text = "High Score: " + highScore;
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}
}
