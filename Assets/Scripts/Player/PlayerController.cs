﻿/*-----------------------------------------------------*/
/*                 PlayerController.cs                 */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /* Public Variables */
    public Vector3 moveInput;                   // Holds the move input
    public Vector3 turnInput;                   // Holds the turn input
    public static bool holdingObject = false;   // True if the player is holding a book

    /* Private Variables */
    private float moveSpeed = 5.5f;             // The move speed modifier
    private float turnSpeed = 120.0f;           // The turn speed modifier
	
    // Use this for initialization
	void Start()
    {

	}
	
	// Update is called once per frame
	void Update()
    {
        // Function call
        MovePlayer();
    }

    /// <summary>
    /// Function for player movement
    /// </summary>
    public void MovePlayer()
    {
        // Check to see of the vertical input has been received
        if (Input.GetAxisRaw("Vertical") == 1)
        {
            // Translate the player forward
            transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
        }
        else if (Input.GetAxisRaw("Vertical") == -1)
        {
            // Translate the player back
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }

        // Check to see of the horizontal input has been received
        if (Input.GetAxisRaw("Horizontal") == 1)
        {
            // Translate the player right
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
        else if (Input.GetAxisRaw("Horizontal") == -1)
        {
            // Translate the player left
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }

        // Check to see if the turn input has been received
        if (Input.GetAxisRaw("Turn") == 1)
        {
            // Rotate the player right
            transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime);
        }
        else if (Input.GetAxisRaw("Turn") == -1)
        {
            // Rotate the player left
            transform.Rotate(Vector3.down * turnSpeed * Time.deltaTime);
        }
    }
}
