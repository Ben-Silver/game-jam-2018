﻿/*-----------------------------------------------------*/
/*                      Compass.cs                     */
/*-----------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    /* Public Variables */
    public Vector3 enemyDirection;
    public Quaternion missionDirection;
    public Transform playerTransform;
    public Transform missionTransform;
    public RectTransform enemyLayer;
    public RectTransform missionLayer;

	// Use this for initialization
	void Start()
    {
        

    }
	
	// Update is called once per frame
	void Update()
    {
        // 
        ChangeEnemyDirection();

        // 
        ChangeMissionDirection();
    }

    /// <summary>
    /// 
    /// </summary>
    public void ChangeEnemyDirection()
    {
        // 
        enemyDirection.z = playerTransform.eulerAngles.y;

        // 
        enemyLayer.localEulerAngles = enemyDirection;
    }

    /// <summary>
    /// 
    /// </summary>
    public void ChangeMissionDirection()
    {
        // Temp
        Vector3 direction = transform.position - missionTransform.position;

        // 
        missionDirection = Quaternion.LookRotation(direction);

        // 
        missionDirection.z = -missionDirection.y;

        // 
        missionDirection.x = 0;

        // 
        missionDirection.y = 0;

        // 
        missionLayer.localRotation = missionDirection * Quaternion.Euler(enemyDirection);
    }
}
